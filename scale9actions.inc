<?php
/**
 * @file
 * ImageCache action hook implementations.
 *
 * Based on code from the imageapi and imagecache_actions module projects.
 *
 * @author Eric de Groot (http://ericdegroot.com)
 */

/**
 * ImageCache Resize (scale-9)
 *
 * @param $action
 *   An array of data for this action.
 * @return
 *   The form definition.
 */
function imagecache_scale9actions_resize_form($action) {
  if (imageapi_default_toolkit() != 'imageapi_gd') {
    drupal_set_message(t('This effect requires GD image toolkit only.'), 'warning');
  }

  $defaults = array(
    'width' => '',
    'height' => '',
    'scale9' => array(
      'left' => '',
      'top' => '',
      'right' => '',
      'bottom' => '',
    ),
  );

  $action = array_merge($defaults, (array)$action);

  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => isset($action['width']) ? $action['width'] : '100%',
    '#description' => t('Enter a width in pixels or as a percentage. i.e. 500 or 80%.'),
  );

  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => isset($action['height']) ? $action['height'] : '100%',
    '#description' => t('Enter a height in pixels or as a percentage. i.e. 500 or 80%.'),
  );

  $form['scale9'] = array(
    '#type' => 'fieldset',
    '#title' => 'Scale-9',
    'help' => array(
      '#type' => 'markup',
      '#value' => '<p>'. t('Set the scale-9 values.') .'</p>',
    ),
    'left' => array(
      '#type' => 'textfield',
      '#title' => t('left'),
      '#default_value' => $action['scale9']['left'],
      '#description' => t('Enter a value in pixels'),
      '#size' => 5,
    ),
    'top' => array(
      '#type' => 'textfield',
      '#title' => t('top'),
      '#default_value' => $action['scale9']['top'],
      '#description' => t('Enter a value in pixels'),
      '#size' => 5,
    ),
    'right' => array(
      '#type' => 'textfield',
      '#title' => t('right'),
      '#default_value' => $action['scale9']['right'],
      '#description' => t('Enter a value in pixels'),
      '#size' => 5,
    ),
    'bottom' => array(
      '#type' => 'textfield',
      '#title' => t('bottom'),
      '#default_value' => $action['scale9']['bottom'],
      '#description' => t('Enter a value in pixels'),
      '#size' => 5,
    ),
  );

  $form['#submit'][] = 'imagecache_scale9actions_resize_form_submit';

  return $form;
}

/**
 * ImageCache Resize (scale-9) imagecache_ui theme.
 *
 * @param $element
 *   The UI element for this action.
 * @return A string representation of the action data.
 */
function theme_imagecache_scale9actions_resize($element) {
  $action = $element['#value'];

  $output = 'width: '. $action['width'] .', height: '. $action['height'];

  if ($action['scale9']['left']) {
    $output .= ', left: '. $action['scale9']['left'];
  }

  if ($action['scale9']['top']) {
    $output .= ', top: '. $action['scale9']['top'];
  }

  if ($action['scale9']['right']) {
    $output .= ', right: '. $action['scale9']['right'];
  }

  if ($action['scale9']['bottom']) {
    $output .= ', bottom: '. $action['scale9']['bottom'];
  }

  return $output;
}

/**
 * ImageCache Resize (scale-9) action implementation.
 *
 * @param $image
 *   The image.
 * @param $action
 *   An array of data for this action.
 * @return TRUE if the action is successful.
 */
function imagecache_scale9actions_resize_image(&$image, $action = array()) {
  if (!imagecache_scale9actions_image_resize($image, $action['width'], $action['height'],
    $action['scale9']['left'], $action['scale9']['top'], $action['scale9']['right'],
    $action['scale9']['bottom'])
  ) {
    watchdog('imagecache_scale9actions', 'imagecache_scale9actions_resize_image failed. image: %image, data: %data.',
      array('%path' => $image, '%data' => print_r($action, TRUE)), WATCHDOG_ERROR);

    return FALSE;
  }

  return TRUE;
}

/**
 * ImageCache Scale (scale-9) form.
 *
 * @param $action
 *   An array of data for this action.
 * @return
 *   The form definition.
 */
function imagecache_scale9actions_scale_form($action) {
  if (imageapi_default_toolkit() != 'imageapi_gd') {
    drupal_set_message(t('This effect requires GD image toolkit only.'), 'warning');
  }

  $form = imagecache_scale9actions_resize_form($action);
  $form['upscale'] = array(
    '#type' => 'checkbox',
    '#default_value' => (isset($action['upscale'])) ? $action['upscale'] : 0,
    '#title' => t('Allow Upscaling'),
    '#description' => t('Let scale make images larger than their original size'),
  );

  $form['#submit'][] = 'imagecache_scale9actions_scale_form_submit';

  return $form;
}

/**
 * ImageCache Scale (scale-9) imagecache_ui theme.
 *
 * @param $element
 *   The UI element for this action.
 * @return A string representation of the action data.
 */
function theme_imagecache_scale9actions_scale($element) {
  $action = $element['#value'];

  $output = 'width: '. $action['width'] .', height: '. $action['height'];
  $output .= ', upscale: '. (($element['#value']['upscale']) ? t('Yes') : t('No'));

  if ($action['scale9']['left']) {
    $output .= ', left: '. $action['scale9']['left'];
  }

  if ($action['scale9']['top']) {
    $output .= ', top: '. $action['scale9']['top'];
  }

  if ($action['scale9']['right']) {
    $output .= ', right: '. $action['scale9']['right'];
  }

  if ($action['scale9']['bottom']) {
    $output .= ', bottom: '. $action['scale9']['bottom'];
  }

  return $output;
}

/**
 * ImageCache Scale (scale-9) action implementation.
 *
 * @param $image
 *   The image.
 * @param $action
 *   An array of data for this action.
 * @return TRUE if the action is successful.
 */
function imagecache_scale9actions_scale_image(&$image, $action = array()) {
  // Set impossibly large values if the width and height aren't set.
  $action['width'] = $action['width'] ? $action['width'] : 9999999;
  $action['height'] = $action['height'] ? $action['height'] : 9999999;

  if (!imagecache_scale9actions_image_scale($image, $action['width'], $action['height'], $action['upscale'],
    $action['scale9']['left'], $action['scale9']['top'], $action['scale9']['right'],
    $action['scale9']['bottom'])
  ) {
    watchdog('imagecache', 'imagecache_scale9actions_scale_image failed. image: %image, data: %data.',
      array('%image' => $image->source, '%data' => print_r($action, TRUE)), WATCHDOG_ERROR);

    return FALSE;
  }

  return TRUE;
}

/**
 * ImageCache Overlay (scale-9)
 *
 * @param $action
 *   An array of data for this action.
 * @return
 *   The form definition.
 */
function imagecache_scale9actions_overlay_form($action) {
  if (imageapi_default_toolkit() != 'imageapi_gd') {
    drupal_set_message(t('This effect requires GD image toolkit only.'), 'warning');
  }

  $defaults = array(
    'alpha' => '100',
    'path' => '',
    'under' => FALSE,
    'scale9' => array(
      'left' => '',
      'top' => '',
      'right' => '',
      'bottom' => '',
    ),
    'exact' => array( 
      'width' => '',
      'height' => '',
      'xpos' => '',
      'ypos' => '',
    ),
    'relative' => array(
      'leftdiff' => '',
      'rightdiff' => '',
      'topdiff' => '',
      'bottomdiff' => '',
    ),
  );
  $action = array_merge($defaults, (array)$action);

  $form = array();

  $form['alpha'] = array(
    '#type' => 'textfield',
    '#title' => t('opacity'),
    '#default_value' => $action['alpha'],
    '#size' => 6,
    '#description' => t('Opacity: 0-100. Be aware that values other than 100% may be slow to process.'),
  );

  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('file name'),
    '#default_value' => $action['path'],
    '#description' => t("File may be in the 'files/' folder, or relative to the Drupal siteroot."),
    '#element_validate' => array('imagecache_scale9actions_overlay_validate_file'),
  );

  $form['under'] = array(
    '#type' => 'checkbox',
    '#title' => t('Place overlay <em>under</em> image'),
    '#default_value' => $action['under'],
  );

  $form['scale9'] = array(
    '#type' => 'fieldset',
    '#title' => 'Scale-9',
    'help' => array(
      '#type' => 'markup',
      '#value' => '<p>'. t('Set the scale-9 values.') .'</p>',
    ),
    'left' => array(
      '#type' => 'textfield',
      '#title' => t('left'),
      '#default_value' => $action['scale9']['left'],
      '#description' => t('Enter a value in pixels'),
      '#size' => 5,
    ),
    'top' => array(
      '#type' => 'textfield',
      '#title' => t('top'),
      '#default_value' => $action['scale9']['top'],
      '#description' => t('Enter a value in pixels'),
      '#size' => 5,
    ),
    'right' => array(
      '#type' => 'textfield',
      '#title' => t('right'),
      '#default_value' => $action['scale9']['right'],
      '#description' => t('Enter a value in pixels'),
      '#size' => 5,
    ),
    'bottom' => array(
      '#type' => 'textfield',
      '#title' => t('bottom'),
      '#default_value' => $action['scale9']['bottom'],
      '#description' => t('Enter a value in pixels'),
      '#size' => 5,
    ),
  );

  $form['info'] = array(
    '#value' => t('Enter values in ONLY ONE of the below options. Either exact or relative.'.
      ' Most values are optional - you can adjust only one dimension as needed.'.
      ' If no useful values are set, the current base image size will be used.'),
  );
  $form['exact'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => 'Exact size',
    'help' => array(
      '#type' => 'markup',
      '#value' => '<p>'. t('Set the overlay to a precise size.') .'</p>',
    ),
    'xpos' => array(
      '#type' => 'textfield',
      '#title' => t('X offset'),
      '#default_value' => $action['exact']['xpos'],
      '#size' => 6,
      '#description' => t('Enter an offset in pixels or use a keyword: <em>left</em>,'.
        ' <em>center</em>, or <em>right</em>.'),
    ),
    'ypos' => array(
      '#type' => 'textfield',
      '#title' => t('Y offset'),
      '#default_value' => $action['exact']['ypos'],
      '#size' => 6,
      '#description' => t('Enter an offset in pixels or use a keyword: <em>top</em>,'.
        ' <em>center</em>, or <em>bottom</em>.'),
    ),
    'width' => array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => $action['exact']['width'],
      '#description' => t('Enter a value in pixels or percent'),
      '#size' => 5,
    ),
    'height' => array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#default_value' => $action['exact']['height'],
      '#description' => t('Enter a value in pixels or percent'),
      '#size' => 5,
    ),
  );

  if (!$action['exact']['xpos'] && !$action['exact']['ypos'] && !$action['exact']['width'] &&
    !$action['exact']['height']
  ) {
    $form['exact']['#collapsed'] = TRUE;
  }

  $form['relative'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('Relative size'),
    'help' => array(
      '#type' => 'markup',
      '#value' => '<p>'. t('Set the overlay to a relative size, based on the current image dimensions.') .'</p>',
    ),
    'leftdiff' => array(
      '#type' => 'textfield',
      '#title' => t('left difference'),
      '#default_value' => $action['relative']['leftdiff'],
      '#size' => 6,
      '#description' => t('Enter an offset in pixels.'),
    ),
    'rightdiff' => array(
      '#type' => 'textfield',
      '#title' => t('right difference'),
      '#default_value' => $action['relative']['rightdiff'],
      '#size' => 6,
      '#description' => t('Enter an offset in pixels.'),
    ),
    'topdiff' => array(
      '#type' => 'textfield',
      '#title' => t('top difference'),
      '#default_value' => $action['relative']['topdiff'] ,
      '#size' => 6,
      '#description' => t('Enter an offset in pixels.'),
    ),
    'bottomdiff' => array(
      '#type' => 'textfield',
      '#title' => t('bottom difference'),
      '#default_value' => $action['relative']['bottomdiff'],
      '#size' => 6,
      '#description' => t('Enter an offset in pixels.'),
    ),
  );

  if (!$action['relative']['leftdiff'] && !$action['relative']['rightdiff'] &&
    !$action['relative']['topdiff'] && !$action['relative']['bottomdiff']
  ) {
    $form['relative']['#collapsed'] = TRUE;
  }

  $form['#submit'][] = 'imagecache_scale9actions_overlay_form_submit';

  return $form;
}

/**
 * ImageCache Overlay (scale-9) form file validation.
 *
 * @param $element
 *   The form element.
 * @param $form_state
 *   The form state.
 */
function imagecache_scale9actions_overlay_validate_file(&$element, &$form_state) {
  if (!file_exists($element['#value']) && !file_exists(file_create_path($element['#value']))) {
    form_error($element, t("Unable to find the named file '%filepath' in either the site or the files directory.'.
      ' Please check the path. Use relative paths only.", array('%filepath' => $element['#value'])));
  }
}

/**
 * ImageCache Overlay (scale-9) action implementation.
 *
 * @param $image
 *   The image.
 * @param $action
 *   An array of data for this action.
 * @return TRUE if the action is successful.
 */
function imagecache_scale9actions_overlay_image(&$image, $action = array()) {
  // search for full (siteroot) paths, then file dir paths, then relative to the current theme
  if (file_exists($action['path'])) {
    $overlay = imageapi_image_open($action['path'], $image->toolkit);
  }
  else if (file_exists(file_create_path($action['path']))) {
    $overlay = imageapi_image_open(file_create_path($action['path']), $image->toolkit);
  }

  if (!isset($overlay) || !$overlay) {
    trigger_error(t("Failed to find overlay image %path for imagecache_actions file-to-canvas action.'.
      ' File was not found in the sites 'files' path or the current theme folder.",
      array('%path' => $action['path'])), E_USER_WARNING);

    return FALSE;
  }

  $xpos = 0;
  $ypos = 0;
  $width = 0;
  $height = 0;
  if ($action['exact']['xpos'] || $action['exact']['ypos'] ||
    $action['exact']['width'] || $action['exact']['height']
  ) {
    if (!$action['exact']['width'])
      $action['exact']['width'] = $image->info['width'];

    if (!$action['exact']['height'])
      $action['exact']['height'] = $image->info['height'];

    $width = _imagecache_percent_filter($action['exact']['width'],
      $image->info['width']);
    $height = _imagecache_percent_filter($action['exact']['height'],
      $image->info['height']);

    $xpos = _imagecache_keyword_filter($action['exact']['xpos'],
      $image->info['width'], $width);
    $ypos = _imagecache_keyword_filter($action['exact']['ypos'],
      $image->info['height'], $height);
  }
  else {
    $width = $image->info['width'] - $action['relative']['leftdiff'] +
      $action['relative']['rightdiff'];
    $height = $image->info['height'] - $action['relative']['topdiff'] +
      $action['relative']['bottomdiff'];

    $xpos = $action['relative']['leftdiff'];
    $ypos = $action['relative']['topdiff'];
  }

  if (!imagecache_scale9actions_image_resize($overlay, $width, $height, $action['scale9']['left'],
    $action['scale9']['top'], $action['scale9']['right'], $action['scale9']['bottom'])
  ) {
    return FALSE;
  }

  if ($action['under']) {
    // ensure dependencies are loaded.
    imagecache_include_standard_actions();
    module_load_include('inc', 'imagecache_canvasactions', 'canvasactions');

    $crop_rules = array(
      'xoffset' => max(0, -($xpos)),
      'yoffset' => max(0, -($ypos)),
      'width' => min($width + min(0, $xpos), $image->info['width'] - max(0, $xpos)),
      'height' => min($height + min(0, $ypos), $image->info['height'] - max(0, $ypos)),
    );
    if (!imagecache_crop_image($overlay, $crop_rules)) {
      return FALSE;
    }

    $definecanvas_rules = array(
      'under' => TRUE,
      'exact' => array(
        'xpos' => min($image->info['height'], max(0, $xpos)),
        'ypos' => min($image->info['width'], max(0, $ypos)),
        'width' => $image->info['width'],
        'height' => $image->info['height'],
      ),
    );
    if (!canvasactions_definecanvas_image($overlay, $definecanvas_rules)) {
      return FALSE;
    }

    if (imageapi_image_overlay($overlay, $image, 0, 0, $action['alpha'], TRUE)) {
      $image->resource = $overlay->resource;
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  else {
    return imageapi_image_overlay($image, $overlay, $xpos, $ypos, $action['alpha']);
  }
}

/**
 * ImageCache Overlay (scale-9) imagecache_ui theme.
 *
 * @param $element
 *   The UI element for this action.
 * @return A string representation of the action data.
 */
function theme_imagecache_scale9actions_overlay($element) {
  $action = $element['#value'];

  $output = '<b>'. basename($action['path']) .'</b>';

  if ($action['scale9']['left']) {
    $output .= ', left: '. $action['scale9']['left'];
  }

  if ($action['scale9']['top']) {
    $output .= ', top: '. $action['scale9']['top'];
  }

  if ($action['scale9']['right']) {
    $output .= ', right: '. $action['scale9']['right'];
  }

  if ($action['scale9']['bottom']) {
    $output .= ', bottom: '. $action['scale9']['bottom'];
  }

  return $output;
}

/**
 * ImageCache Random Switcher
 *
 * @param $action
 *   An array of data for this action.
 * @return
 *   The form definition.
 */
function imagecache_scale9actions_random_form($action) {
  $defaults = array();
  $action = array_merge($defaults, (array)$action);

  $form = array();

  $presets = array();
  foreach (imagecache_presets(TRUE) as $preset) {
    $presets[$preset['presetid']] = $preset['presetname'];
  }

  $form['presets'] = array(
    '#type' => 'select',
    '#title' => t('Presets'),
    '#options' => $presets,
    '#default_value' => $action['presets'],
    '#multiple' => TRUE,
    '#size' => 5,
  );

  return $form;
}

/**
 * ImageCache Random Switcher imagecache_ui theme.
 *
 * @param $element
 *   The UI element for this action.
 * @return A string representation of the action data.
 */
function theme_imagecache_scale9actions_random($element) {
  $action = $element['#value'];
  return 'presets: '. count($action['presets']);
}

/**
 * ImageCache Random Switcher action implementation.
 *
 * @param $image
 *   The image.
 * @param $action
 *   An array of data for this action.
 * @return TRUE if the action is successful.
 */
function imagecache_scale9actions_random_image(&$image, $action = array()) {
  $preset_id = array_rand($action['presets']);
  if ($preset_id) {
    $preset = imagecache_preset($preset_id);

    if ($preset) {
      foreach ($preset['actions'] as $sub_action) {
        _imagecache_apply_action($sub_action, $image);
      }
    }
  }

  return TRUE;
}
