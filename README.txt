
Additional actions for imagecache processing using scale-9 formatting. This module also includes an action that randomly switches to another action from a selected list. Based on code from the imageapi and imagecache_actions modules.

Scale-9 formatting lets you define 9 regions of an image that scale independently. The 9 regions are defined by giving left, top, right and bottom values. The left and right values being pixel offsets from the left edge of the image, and the top and bottom values being pixel offsets from the top of the image. This results in a 3x3 grid.
